//
//  SharedObject.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import CoreLocation

class SharedObject {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static var locationManager: CLLocationManager?
    static var userLocation : CLLocation?
    static var favourateLocationsList: [FavourateLocation] = [FavourateLocation]()
    static var choosenDeliveryLocation: FavourateLocation?
    static var loginController = LoginController()
    static var registerController = RegisterController()
    static var providerAdminController = ProviderAdminController()
    static var driverAdminController = DriverAdminController()
}
