//
//  DriverServiceCell.swift
//  FreeHands
//
//  Created by Apple on 7/19/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class DriverServiceCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let productPrice: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let priceCurrency: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let providerNameTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let providerName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let clientNameTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let clientName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension DriverServiceCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = .white
        addSubViewsInsideMainViews()
        setProviderNameTitleLabelViewConstraints(control: providerNameTitle)
        setProviderNameLabelViewConstraints(control: providerName)
        setClientNameLabelViewConstraints(control: clientName)
        setClientNameTitleLabelViewConstraints(control: clientNameTitle)
        setPriceLabelViewConstraints(control: productPrice)
        setCurrencyLabelViewConstraints(control: priceCurrency)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(providerName)
        addSubview(providerNameTitle)
        addSubview(clientName)
        addSubview(clientNameTitle)
        addSubview(productPrice)
        addSubview(priceCurrency)
    }
    
    private func setPriceLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: SharedValues.defaultPadding * -1),
            control.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setCurrencyLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: SharedValues.defaultPadding * -1),
            control.leadingAnchor.constraint(equalTo: productPrice.trailingAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setClientNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerName.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: clientNameTitle.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2)
            ])
    }
    
    private func setClientNameTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerNameTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerNameTitle.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2)
            ])
    }
    
    private func setProviderNameTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2)
            ])
    }
    
}
