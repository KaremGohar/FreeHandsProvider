//
//  MainTabBarController.swift
//  FreeHandsProvider
//
//  Created by Apple on 7/17/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultParameters()
        setupControllers()
    }
    
    private func setupDefaultParameters(){
        
    }
    
    private func setupControllers(){
        setTabImagesAndTitles()
        viewControllers = [ProviderAdminController(), DriverAdminController()]
    }
    
    private func setTabImagesAndTitles(){
//        SharedObject.basketCollectionViewController.tabBarItem.title = SharedValues.BasketController.tabbarTitle
//        SharedObject.basketCollectionViewController.tabBarItem.image = SharedValues.BasketController.tabbarIcon
//
//        SharedObject.offerCollectionViewController.tabBarItem.title = SharedValues.OfferController.tabbarTitle
//        SharedObject.offerCollectionViewController.tabBarItem.image = SharedValues.OfferController.tabbarIcon
//
//        SharedObject.orderCollectionViewController.tabBarItem.title = SharedValues.OrderController.tabbarTitle
//        SharedObject.orderCollectionViewController.tabBarItem.image = SharedValues.OrderController.tabbarIcon

        SharedObject.loginController.tabBarItem.title = SharedValues.ProviderController.tabbarTitle
        SharedObject.loginController.tabBarItem.image = SharedValues.ProviderController.tabbarIcon
    }
    
}
